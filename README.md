# SquabDB
SquabDB is a little Pigeon Database intended as a typhoon database engine demonstration. Written in C language.
The graphical interface uses raylib 4.2 and raygui 3.2

![The main form](doc/main.png)

## Requirements
- typhoon database (a C library, that comes with it's own database management tools);
- see it [here](https://gitlab.com/funlw65/osdb-typhoon)
- raylib 4.2 + raygui 3.2 libraries.
- see it [here](https://github.com/raysan5/raylib)
