#ifndef MYRAYGUICONTROLS_H
#define MYRAYGUICONTROLS_H

Font fontSButton;

RAYGUIAPI bool GuiGradientButton(Rectangle bounds, const char *text, bool DarkTheme, bool ButtonActive){
 
  GuiState state = guiState;
  bool IsGuiLocked;
  Color colorstart, colorend;
  Vector2 mousepos, bcoord, myStringSize;
  int x,y,w,h;
  
  bool pressed = false;
  if (ButtonActive) {
    state = GuiGetState();
    IsGuiLocked = GuiIsLocked();
  }
  else {
    state = STATE_DISABLED;
    IsGuiLocked = true;
  }
    
  if ((state != STATE_DISABLED) && (!IsGuiLocked)){
    mousepos = GetMousePosition();
    if(CheckCollisionPointRec(mousepos, bounds)) {
      if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)) state = STATE_PRESSED;
      else state = STATE_FOCUSED;
      if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) pressed = true;
    }
  }
  
  if (DarkTheme) {
    if (state == STATE_NORMAL){ 
      // 
      // 
      colorstart.r = 0x85; 
      colorstart.g = 0x85; 
      colorstart.b = 0x85; 
      colorstart.a = 48;
      //
      colorend.r = 0x38; 
      colorend.g = 0x38; 
      colorend.b = 0x38; 
      colorend.a = 48;
    }
    if (state == STATE_FOCUSED){ 
      //colorstart := elementStyle; // f5ff97
      //colorend := elementStyle;   // 71792b
      colorstart.r = 0xf5; 
      colorstart.g = 0xff; 
      colorstart.b = 0x97; 
      colorstart.a = 96;
      //
      colorend.r = 0x71; 
      colorend.g = 0x79; 
      colorend.b = 0x2b; 
      colorend.a = 96;
    }
    if (state == STATE_PRESSED) { 
      //colorstart := elementStyle; // ffd490
      //colorend := elementStyle;   // b38439
      colorstart.r = 0xff; 
      colorstart.g = 0xd4; 
      colorstart.b = 0x90; 
      colorstart.a = 96;
      //
      colorend.r = 0xb3; 
      colorend.g = 0x84; 
      colorend.b = 0x39; 
      colorend.a = 96;
    }
    if (state == STATE_DISABLED){
      // 
      // 
      colorstart.r = 0x85; 
      colorstart.g = 0x85; 
      colorstart.b = 0x85; 
      colorstart.a = 48;
      //
      colorend.r = 0x38; 
      colorend.g = 0x38; 
      colorend.b = 0x38; 
      colorend.a = 48;
    }
  }
  else {  // LIGHT THEME
    if (state == STATE_NORMAL) { 
      // aeaeae
      // 616161
      colorstart.r = 0xae; 
      colorstart.g = 0xae; 
      colorstart.b = 0xae; 
      colorstart.a = 96;
      //
      colorend.r = 0x61; 
      colorend.g = 0x61; 
      colorend.b = 0x61; 
      colorend.a = 96;
    }
    if (state == STATE_FOCUSED) { 
      //colorstart := elementStyle; // f5ff97
      //colorend := elementStyle;   // 71792b
      colorstart.r = 0xf5; 
      colorstart.g = 0xff; 
      colorstart.b = 0x97; 
      colorstart.a = 192;
      //
      colorend.r = 0x71; 
      colorend.g = 0x79; 
      colorend.b = 0x2b; 
      colorend.a = 192;
    }
    if (state == STATE_PRESSED) {
      //colorstart := elementStyle; // ffd490
      //colorend := elementStyle;   // b38439
      colorstart.r = 0xff; 
      colorstart.g = 0xd4; 
      colorstart.b = 0x90; 
      colorstart.a = 192;
      //
      colorend.r = 0xb3; 
      colorend.g = 0x84; 
      colorend.b = 0x39; 
      colorend.a = 192;
    }
    if (state == STATE_DISABLED) {
      // aeaeae
      // 616161
      colorstart.r = 0xae; 
      colorstart.g = 0xae; 
      colorstart.b = 0xae; 
      colorstart.a = 96;
      //
      colorend.r = 0x61; 
      colorend.g = 0x61; 
      colorend.b = 0x61; 
      colorend.a = 96;
    }
  }

  x = (int)bounds.x;
  y = (int)bounds.y;
  w = (int)bounds.width;
  h = (int)bounds.height;
  DrawRectangleGradientV(x, y, w, h, colorstart, colorend);
  //
  
  if ((state != STATE_DISABLED) && (!IsGuiLocked)){
    myStringSize = MeasureTextEx(guiFont, text, 16, 2);
    bcoord.x = (bounds.width / 2) - (myStringSize.x / 2) + bounds.x;
    bcoord.y = (bounds.height / 2) - (myStringSize.y / 2) + bounds.y;
    DrawTextEx(guiFont, text, bcoord, 16, 2, GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_NORMAL)));
  }
  return pressed;
}

RAYGUIAPI bool GuiSpeedButton(Vector2 sbCoord, float sbWidth, Texture2D activePix, Texture2D disabledPix, bool ButtonActive){ 

  GuiState state = guiState;
  bool IsGuiLocked;
  Rectangle bounds;
  Vector2 mousepos;
  int x,y;

  bounds.x = sbCoord.x;
  bounds.y = sbCoord.y;
  bounds.width = sbWidth;
  bounds.height = sbWidth;
  
  bool pressed = false;
  if (ButtonActive) {
    state = GuiGetState();
    IsGuiLocked = GuiIsLocked();
  }
  else {
    state = STATE_DISABLED;
    IsGuiLocked = true;
  }
  
  if ((state != STATE_DISABLED) && (!IsGuiLocked)){
    mousepos = GetMousePosition();
    if(CheckCollisionPointRec(mousepos, bounds)) {
      if(IsMouseButtonDown(MOUSE_LEFT_BUTTON))  
        state = STATE_PRESSED;
      else 
        state = STATE_FOCUSED;
      if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) pressed = true;
    }  
  }
  
  x = (int)bounds.x;
  y = (int)bounds.y;
  
  if ((state != STATE_DISABLED) && (!IsGuiLocked)){
    switch (state) {
      case STATE_NORMAL: 
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_NORMAL)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_NORMAL)));
        break;
      case STATE_FOCUSED: 
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
        break;  
      case STATE_PRESSED: 
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
        break;  
    }
    DrawTexture(activePix, x+2, y+2, WHITE);
  }  
  else {
    //DrawRectangle(x+2,y+2,32,32,GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_DISABLED)));
    DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_DISABLED)));
    DrawTexture(disabledPix, x+2, y+2, WHITE);
  }
  
  
  return pressed;
}

RAYGUIAPI bool GuiSpeedButtonLbl(Vector2 sbCoord, float sbWidth, const char *text, Texture2D activePix, Texture2D disabledPix, bool ButtonActive){ 

  GuiState state = guiState;
  bool IsGuiLocked;
  Rectangle bounds;
  Vector2 mousepos, bcoord;
  int x,y;

  bounds.x = sbCoord.x;
  bounds.y = sbCoord.y;
  bounds.width = sbWidth;
  bounds.height = sbWidth;
  
  bool pressed = false;
  if (ButtonActive) {
    state = GuiGetState();
    IsGuiLocked = GuiIsLocked();
  }
  else {
    state = STATE_DISABLED;
    IsGuiLocked = true;
  }
  
  if ((state != STATE_DISABLED) && (!IsGuiLocked)){
    mousepos = GetMousePosition();
    if(CheckCollisionPointRec(mousepos, bounds)) {
      if(IsMouseButtonDown(MOUSE_LEFT_BUTTON))  
        state = STATE_PRESSED;
      else 
        state = STATE_FOCUSED;
      if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) pressed = true;
    }  
  }
  
  x = (int)bounds.x;
  y = (int)bounds.y;
  
  if ((state != STATE_DISABLED) && (!IsGuiLocked)){
    switch (state) {
      case STATE_NORMAL: 
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_NORMAL)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_NORMAL)));
        break;
      case STATE_FOCUSED: 
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_FOCUSED)));
        break;  
      case STATE_PRESSED: 
        //DrawRectangle(x+2,y+2,32,32, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
        DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_PRESSED)));
        break;  
    }
    DrawTexture(activePix, x+2, y+2, WHITE);
  }  
  else {
    //DrawRectangle(x+2,y+2,32,32,GetColor(GuiGetStyle(DEFAULT, BASE_COLOR_DISABLED)));
    DrawRectangleRoundedLines(bounds, 0.2, 4, 2, GetColor(GuiGetStyle(DEFAULT, BORDER_COLOR_DISABLED)));
    DrawTexture(disabledPix, x+2, y+2, WHITE);
  }
  bcoord.x = sbCoord.x + 2;
  bcoord.y = sbCoord.y + 39;
  DrawTextEx(fontSButton, text, bcoord, 14, 2, GetColor(GuiGetStyle(DEFAULT, TEXT_COLOR_NORMAL)));  
  
  return pressed;
}


#endif //MYRAYGUICONTROLS_H
